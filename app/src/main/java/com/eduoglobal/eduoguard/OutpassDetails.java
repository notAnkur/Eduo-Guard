package com.eduoglobal.eduoguard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.eduoglobal.eduoguard.Model.Outpass;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class OutpassDetails extends AppCompatActivity {

//	private ProgressBar spinner;
	ImageView profileImageView;
	TextView nameTextView, outpassStatusTextView, regNumberTextView, contactTextView;

	private Outpass outpass;
	protected boolean isTokenValid = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_outpass_details);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		profileImageView = findViewById(R.id.profile_image);
		nameTextView = findViewById(R.id.name);
		outpassStatusTextView = findViewById(R.id.outpassStatus);
		regNumberTextView = findViewById(R.id.regNumber);
		contactTextView = findViewById(R.id.contact);

		SharedPreferences userDetails = getSharedPreferences("UserProfile", MODE_PRIVATE);
		final String userToken = userDetails.getString("token", "");

		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Updating entry/exit logs...", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
				if(isTokenValid) {
					updateEntryExit(outpass, userToken);
				} else {
					Intent scannerIntent = new Intent(OutpassDetails.this, MainActivity.class);
					startActivity(scannerIntent);
					//kill em
					finish();
				}
			}
		});

		Intent intent = getIntent();
		String outpassToken = intent.getStringExtra("JWT");
//		Toast.makeText(this, outpassToken, Toast.LENGTH_SHORT).show();

		fetchOutpass(outpassToken, userToken);
	}

	private void fetchOutpass(String outpassToken, final String userToken) {
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(this);
			String URL = "https://api.iar.ankuranant.dev/outpass/qr";
			JSONObject jsonBody = new JSONObject();
			//TODO: Replace with jsonwebtoken
			jsonBody.put("outpassToken", outpassToken);
			final String mRequestBody = jsonBody.toString();

			StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					try {
						//server response
						Log.i("LOG_VOLLEY", response);
						JSONObject responseJson = new JSONObject(response);
						outpass = new Outpass(
								responseJson.getString("name"),
								responseJson.getString("avatarUrl"),
								responseJson.getString("regNumber"),
								responseJson.getString("contact"),
								responseJson.getString("destination"),
								responseJson.getString("outpassStatus"),
								responseJson.getString("outpassId"));

						handleUI(outpass);
						isTokenValid = true;

					} catch(JSONException jsonEx) {
						jsonEx.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e("LOG_VOLLEY", error.toString());
					Toast.makeText(getApplicationContext(), "Invalid token", Toast.LENGTH_LONG).show();
					nameTextView.setText("Invalid Token");
					nameTextView.setTextColor(Color.RED);
					isTokenValid = false;
				}
			}) {
				@Override
				public String getBodyContentType() {
					return "application/json; charset=utf-8";
				}

				@Override
				public byte[] getBody() throws AuthFailureError {
					try {
						return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
					} catch (UnsupportedEncodingException uee) {
						VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
						return null;
					}
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();
					headers.put("Authorization", "Bearer " + userToken);
					return headers;
				}

				@Override
				protected Response<String> parseNetworkResponse(NetworkResponse response) {
					String responseString = "";
					if (response != null) {

						responseString = String.valueOf(response.statusCode);

					}
					return super.parseNetworkResponse(response);
				}
			};

			requestQueue.add(stringRequest);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void handleUI(Outpass outpass) {
		Toast.makeText(this, "test22", Toast.LENGTH_SHORT).show();
		Glide.with(OutpassDetails.this)
				.load(outpass.getAvatarUrl())
				.placeholder(R.drawable.user_avatar_holder)
				.into(profileImageView);

		nameTextView.setText(outpass.getName());
		outpassStatusTextView.setText(outpass.getOutpassStatus());
		regNumberTextView.setText(outpass.getRegNumber());
		contactTextView.setText(outpass.getContact());

	}

	private void updateEntryExit(Outpass outpass, final String userToken) {
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(this);
			String URL = "https://api.iar.ankuranant.dev/entryexit";
			JSONObject jsonBody = new JSONObject();
			//TODO: Replace with jsonwebtoken
			jsonBody.put("name", outpass.getName());
			jsonBody.put("outpassId", outpass.getOutpassId());
			jsonBody.put("regNumber", outpass.getRegNumber());
			jsonBody.put("contact", outpass.getContact());
			jsonBody.put("destination", outpass.getDestination());
			final String mRequestBody = jsonBody.toString();

			StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					try {
						//server response
						Log.i("LOG_VOLLEY", response);
						JSONObject responseJson = new JSONObject(response);
						Intent scannerIntent = new Intent(OutpassDetails.this, MainActivity.class);
						startActivity(scannerIntent);
						//kill em
						finish();

					} catch(JSONException jsonEx) {
						jsonEx.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e("LOG_VOLLEY", error.toString());
					Toast.makeText(OutpassDetails.this, "Error: Try again", Toast.LENGTH_SHORT).show();
				}
			}) {
				@Override
				public String getBodyContentType() {
					return "application/json; charset=utf-8";
				}

				@Override
				public byte[] getBody() throws AuthFailureError {
					try {
						return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
					} catch (UnsupportedEncodingException uee) {
						VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
						return null;
					}
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();
					headers.put("Authorization", "Bearer " + userToken);
					return headers;
				}

				@Override
				protected Response<String> parseNetworkResponse(NetworkResponse response) {
					String responseString = "";
					if (response != null) {

						responseString = String.valueOf(response.statusCode);

					}
					return super.parseNetworkResponse(response);
				}
			};

			requestQueue.add(stringRequest);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
