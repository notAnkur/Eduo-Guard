package com.eduoglobal.eduoguard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.eduoglobal.eduoguard.Model.QRToken;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class MainActivity extends AppCompatActivity {

	private CodeScanner mCodeScanner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		// Permission handling
		Dexter.withActivity(this)
				.withPermission(Manifest.permission.CAMERA)
				.withListener(new PermissionListener() {
					@Override
					public void onPermissionGranted(PermissionGrantedResponse response) {
						startScanning();
					}

					@Override
					public void onPermissionDenied(PermissionDeniedResponse response) {
						Toast.makeText(MainActivity.this, "This app won\'t  function without this permission.", Toast.LENGTH_LONG).show();
					}

					@Override
					public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
						token.continuePermissionRequest();
					}
				})
				.check();

	}

	@Override
	protected void onResume() {
		super.onResume();
		if(mCodeScanner != null) {
			mCodeScanner.startPreview();
		}
	}

	@Override
	protected void onPause() {
		if(mCodeScanner != null) {
			mCodeScanner.releaseResources();
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private void startScanning() {
		CodeScannerView scannerView = findViewById(R.id.scanner_view);
		mCodeScanner = new CodeScanner(this, scannerView);
		mCodeScanner.setScanMode(ScanMode.SINGLE);

		mCodeScanner.setDecodeCallback(new DecodeCallback() {
			@Override
			public void onDecoded(@NonNull final Result result) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						QRToken qrToken = new QRToken(result.getText());
						Intent outpassDetailsIntent = new Intent(MainActivity.this, OutpassDetails.class);
						outpassDetailsIntent.putExtra("JWT", qrToken.getJwtToken());
						startActivity(outpassDetailsIntent);
					}
				});
			}
		});
		scannerView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mCodeScanner.startPreview();
			}
		});
	}

}
