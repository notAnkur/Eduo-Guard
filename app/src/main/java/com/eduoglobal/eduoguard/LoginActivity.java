package com.eduoglobal.eduoguard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

	private EditText emailEditText;
	private EditText passEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		// Address the email and password field
		emailEditText = (EditText) findViewById(R.id.username);
		passEditText = (EditText) findViewById(R.id.password);

		// If token is saved in shared prefs -> auto login
		SharedPreferences userDetails = getSharedPreferences("UserProfile", MODE_PRIVATE);
		final String userToken = userDetails.getString("token", "");

		if(!userToken.equals("")) {
			Toast.makeText(this, "User found ", Toast.LENGTH_SHORT).show();
			verifyOldToken(userToken);
		}

	}

	public void checkLogin(View arg0) {

		final String email = emailEditText.getText().toString();
		if (!isValidEmail(email)) {
			//Set error message for email field
			emailEditText.setError("Invalid Email");
		}

		final String pass = passEditText.getText().toString();
		if (!isValidPassword(pass)) {
			//Set error message for password field
			passEditText.setError("Password cannot be empty");
		}

		if (isValidEmail(email) && isValidPassword(pass)) {
			// handle login
			Toast.makeText(this, "yolo", Toast.LENGTH_SHORT).show();
			loginUser(email, pass);
		}

	}

	// email validation
	private boolean isValidEmail(String email) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	// password validation
	private boolean isValidPassword(String pass) {
		if (pass != null) {
			return !pass.equals("");
		}
		return false;
	}

	private void loginUser(String emailId, String password) {
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(this);

			String URL = "https://api.iar.ankuranant.dev/auth";

			JSONObject jsonBody = new JSONObject();
			jsonBody.put("emailId", emailId);
			jsonBody.put("password", password);
			final String mRequestBody = jsonBody.toString();

			StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					try {
						//server response
						Log.i("LOG_VOLLEY", response);
						JSONObject responseJson = new JSONObject(response);
						Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();

						//add token to shared prefs
						SharedPreferences userDetails = getSharedPreferences("UserProfile", MODE_PRIVATE);
						SharedPreferences.Editor userLoginEdit = userDetails.edit();
						userLoginEdit.putString("token", responseJson.getString("token"));

						// save to shared prefs
						userLoginEdit.apply();

						Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
						mainIntent.putExtra("token", responseJson.getString("token"));
						startActivity(mainIntent);
						//kill activity after successful login
						finish();
					} catch(JSONException jsonEx) {
						jsonEx.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e("LOG_VOLLEY", error.toString());
					Toast.makeText(getApplicationContext(), "Invalid username or password!", Toast.LENGTH_LONG).show();
				}
			}) {
				@Override
				public String getBodyContentType() {
					return "application/json; charset=utf-8";
				}

				@Override
				public byte[] getBody() throws AuthFailureError {
					try {
						return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
					} catch (UnsupportedEncodingException uee) {
						VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
						return null;
					}
				}

				@Override
				protected Response<String> parseNetworkResponse(NetworkResponse response) {
					String responseString = "";
					if (response != null) {

						responseString = String.valueOf(response.statusCode);

					}
					return super.parseNetworkResponse(response);
				}
			};

			requestQueue.add(stringRequest);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	// Volley POST
	private void verifyOldToken(final String token) {
		try {
			RequestQueue requestQueue = Volley.newRequestQueue(this);
			String URL = "https://api.iar.ankuranant.dev/auth/verifyoldtoken";
			JSONObject jsonBody = new JSONObject();
			jsonBody.put("token", token);
			final String mRequestBody = jsonBody.toString();

			StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					try {
						//server response
						Log.i("LOG_VOLLEY", response);
						JSONObject responseJson = new JSONObject(response);
						autoLogIn(responseJson.getBoolean("isLoggedIn"));
					} catch(JSONException jsonEx) {
						jsonEx.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e("LOG_VOLLEY", error.toString());
					Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_LONG).show();
				}
			}) {
				@Override
				public String getBodyContentType() {
					return "application/json; charset=utf-8";
				}

				@Override
				public byte[] getBody() throws AuthFailureError {
					try {
						return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
					} catch (UnsupportedEncodingException uee) {
						VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
						return null;
					}
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();
					headers.put("Authorization", "Bearer " + token);
					return headers;
				}

				@Override
				protected Response<String> parseNetworkResponse(NetworkResponse response) {
					String responseString = "";
					if (response != null) {

						responseString = String.valueOf(response.statusCode);

					}
					return super.parseNetworkResponse(response);
				}
			};

			requestQueue.add(stringRequest);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	private void autoLogIn(boolean isLoggedIn) {
		Log.i("LOG_VOLLEY", isLoggedIn+"");
		//JSONObject tokenStatus = new JSONObject(response);
		if(isLoggedIn) {
			Log.i("LOG_VOLLEY", "changing");
			Intent i = new Intent(LoginActivity.this, MainActivity.class);
			startActivity(i);
			// kill login activity
			finish();
		}
	}

}
