package com.eduoglobal.eduoguard.Model;

public class EntryExit {
	private String name, regNumber, contact, roomNumber;

	public EntryExit(String name, String regNumber, String contact, String roomNumber) {
		this.name = name;
		this.regNumber = regNumber;
		this.contact = contact;
		this.roomNumber = roomNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
}
