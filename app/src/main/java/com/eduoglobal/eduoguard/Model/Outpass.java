package com.eduoglobal.eduoguard.Model;

public class Outpass {
	private String name, avatarUrl, regNumber, contact, destination, outpassStatus, outpassId;

	public Outpass(String name, String avatarUrl, String regNumber, String contact, String destination, String outpassStatus, String outpassId) {
		this.name = name;
		this.avatarUrl = avatarUrl;
		this.regNumber = regNumber;
		this.contact = contact;
		this.destination = destination;
		this.outpassStatus = outpassStatus;
		this.outpassId = outpassId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getOutpassStatus() {
		return outpassStatus;
	}

	public void setOutpassStatus(String outpassStatus) {
		this.outpassStatus = outpassStatus;
	}

	public String getOutpassId() {
		return outpassId;
	}

	public void setOutpassId(String outpassId) {
		this.outpassId = outpassId;
	}
}
