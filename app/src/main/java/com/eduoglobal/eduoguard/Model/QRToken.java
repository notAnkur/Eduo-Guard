package com.eduoglobal.eduoguard.Model;

public class QRToken {
	private String jwtToken;

	public QRToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	public String getJwtToken() {
		return jwtToken;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}
}
